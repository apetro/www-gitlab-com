---
layout: markdown_page
title: "Developer Marketing - Community"
---

## Community support ideas

GitLab, Inc. should support local community groups, and help community members organize and meet together. 

User groups can... 

* Connect GitLab users together to share insights on using GitLab and extending and integrating GitLab. 
* Help new users get set up with GitLab by using Meet-up in a box resources
* Access exclusive content and remote-meetings with GitLab contributors or guest speakers through the user group network. 

These are some early-stage ideas: 

### Coming soon: Meet-up in a box

Community leaders who join the program will be given local meet-up support. 

* Meet-up in a box: tips and advice about running user groups
* Presentations and assets to help subject matter experts share GitLab with new users
* Special online event streaming to user groups. 
* Funding support for venues
* Funding support for food such as the traditional user group fare: PIZZA or another healthy option. 

### Proposal: Community grant

* Potential for a fund for community events to help with hosting
* Help connect local user groups with sponsors for events

